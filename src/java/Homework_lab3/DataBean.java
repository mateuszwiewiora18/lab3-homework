/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Homework_lab3;

import java.beans.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author wiwi
 */
public class DataBean implements Serializable {
    
    public static final String PROP_HARMONICMEAN = "harmonicmean";
    public static final String PROP_COUNT = "count";
    public static final String PROP_RANDOM = "random";
    public String harmonicmean;
    public int count;
    public List<String> history;
    public String random;
    public PropertyChangeSupport propertySupport;
    
    public DataBean() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public String getHarmonicmean() {
        return harmonicmean;
    }
    
    public void setHarmonicmean(String value) {
        
    }
    
    public String getCount() {
        return Integer.toString(count);
    }
    
    public void setCount(String value) {
        String oldValue = Integer.toString(count);
        count = Integer.parseInt(value);
        propertySupport.firePropertyChange(PROP_COUNT, oldValue, count);       
    }
    
    public String getRandom() { 
        Random r = new Random();
        String result ="";
        int harmonicmeana = 0;
        int[] arr = new int[count];
        for(int i = 0; i < count; i++)
            arr[i] = r.nextInt(100);
          Arrays.sort(arr);
       
        /*obliczenie średniej harmonicznej*/
        for(int i = 0; i < count; i++)   
        {
            harmonicmeana = harmonicmeana + 1 / arr[i];
            
        }
        harmonicmeana = count / harmonicmeana;
        
        if(history == null)
            history = new ArrayList();
        history.add("<td>" + result + "</td>"+"<td>harmonicmeana: " + harmonicmeana + "</td>");
        harmonicmean = Integer.toString(harmonicmeana);
        return result;
        
    }
    
    public String getHistory() {
        String result = "<table>";
        for(int i = history.size()-1; i >= 0; i--)
            result += "<tr>" + history.get(i) + "</tr>";
        result += "</table>";
        return result;
    }
    
    public void setHistory(String value) {
        
    }
    
    public void setRandom(String value) {
        
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
}
