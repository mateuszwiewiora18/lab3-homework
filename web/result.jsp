<%-- 
    Document   : result
    Created on : 1 cze 2022, 16:20:10
    Author     : Wiwi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Results</title>
    </head>
    <body>
        <div id="Container">
        <jsp:useBean id="dBean" class="Homework_lab3.DataBean" scope="session" />
        <h1>Podana liczba</h1><p>
        <jsp:setProperty name="dBean" property="count" value="<%= request.getParameter("count")%>" />
        <i><jsp:getProperty name="dBean" property="count" /></i></p><br>      
        <h1>Wylosowane wartości</h1>
        <p><i><jsp:getProperty name="dBean" property="random" /></i></p><br>
        <h1>Wyliczona średnia harmoniczna</h1>
        <p><i><jsp:getProperty name="dBean" property="harmonicmeana" /></i><br></p>
        <h1> Historia</h1> 
        <p><jsp:getProperty name="dBean" property="history" /></p>
        <form action="index.html">
            <input type="submit" id="SButton" value="Powrot">
        </form>
        </div>
    </body>  
</html>
